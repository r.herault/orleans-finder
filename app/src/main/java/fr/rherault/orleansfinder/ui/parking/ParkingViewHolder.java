package fr.rherault.orleansfinder.ui.parking;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fr.rherault.orleansfinder.R;
import fr.rherault.orleansfinder.data.Parking;
import fr.rherault.orleansfinder.data.ParkingData;

class ParkingViewHolder extends RecyclerView.ViewHolder {
    private ArrayList<ParkingData> infoParking;
    public CardView cardView;
    public TextView title;
    public TextView nbPlace;
    public Button btItineraire;
    public ImageView btStar;

    public ParkingViewHolder(View itemView, ArrayList<ParkingData> infoParking) {
        super(itemView);
        this.infoParking = infoParking;
        cardView = itemView.findViewById(R.id.cardView);
        title = itemView.findViewById(R.id.txt_card);
        nbPlace = itemView.findViewById(R.id.txt_nbPlaceWithTotal);
        btItineraire = itemView.findViewById(R.id.bt_itineraire);
        btStar = itemView.findViewById(R.id.bt_star);
        bindClickListener();
    }

    public void bindClickListener() {
        btStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!infoParking.get(getAdapterPosition()).getFavourite()) {
                    btStar.setImageResource(R.drawable.etoile_plein);
                    infoParking.get(getAdapterPosition()).setFavourite(true);
                } else {
                    btStar.setImageResource(R.drawable.etoile_vide);
                    infoParking.get(getAdapterPosition()).setFavourite(false);
                }
            }
        });

        btItineraire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:-1,0?q=" + infoParking.get(getAdapterPosition()).getLatitude() + "," + infoParking.get(getAdapterPosition()).getLongitude() +
                        "( " + "Parking '" + infoParking.get(getAdapterPosition()).getName() + "' )"));
                v.getContext().startActivity(intent);
            }
        });
    }
}
