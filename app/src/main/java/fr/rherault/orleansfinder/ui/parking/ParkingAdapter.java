package fr.rherault.orleansfinder.ui.parking;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

import fr.rherault.orleansfinder.R;
import fr.rherault.orleansfinder.data.Parking;
import fr.rherault.orleansfinder.data.ParkingData;

public class ParkingAdapter extends RecyclerView.Adapter<ParkingViewHolder> {

    ArrayList<ParkingData> infoParking;
    Context context;

    public ParkingAdapter(ArrayList<ParkingData> data, Context context) {
        //super();
        this.context = context;
        this.infoParking = data;
    }

    @Override
    public ParkingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_parking, parent, false);
        ParkingViewHolder viewHolder = new ParkingViewHolder(view, infoParking);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ParkingViewHolder holder, final int position) {
        holder.title.setText(String.format("Nom : %s", infoParking.get(position).getName()));
        holder.nbPlace.setText(String.format("%s/%s", infoParking.get(position).getNbPlace(), infoParking.get(position).getTotal()));
        if (infoParking.get(position).getFavourite()){
            holder.btStar.setImageResource(R.drawable.etoile_plein);
        }
        else{
            holder.btStar.setImageResource(R.drawable.etoile_vide);
        }
    }

    @Override
    public int getItemCount() {
        return this.infoParking.size();
    }
}
