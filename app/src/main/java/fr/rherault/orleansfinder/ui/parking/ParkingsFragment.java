package fr.rherault.orleansfinder.ui.parking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;

import java.util.ArrayList;
import java.util.Objects;

import fr.rherault.orleansfinder.R;
import fr.rherault.orleansfinder.api.ParkingAsync;
import fr.rherault.orleansfinder.data.Parking;
import fr.rherault.orleansfinder.data.ParkingData;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParkingsFragment extends Fragment {
    private TextView responseView;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> rep = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parkings, container, false);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final PullRefreshLayout layout = (PullRefreshLayout) view.findViewById(R.id.refreshLayout);

        layout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);

        Objects.requireNonNull(getActivity()).setTitle("Parkings Orléans");


        layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshParkings(view);
            }
        });
        refreshParkings(view);
    }

    public void refreshParkings(final View view){
        this.recyclerView = view.findViewById(R.id.recyclerView);
        ParkingAsync ats = (ParkingAsync) new ParkingAsync(view, getContext(), new ParkingAsync.AsyncResponse() {
            @Override
            public void processFinish(ArrayList<ParkingData> output) {
                recyclerView = view.findViewById(R.id.recyclerView);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(new ParkingAdapter(output, getContext()));
            }
        });
        ats.execute();
    }
}
