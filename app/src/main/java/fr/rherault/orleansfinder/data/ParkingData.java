package fr.rherault.orleansfinder.data;

import android.content.Context;

public class ParkingData {

    SqlWrapper wrapper;
    private String id;
    Context context;
    private String name;
    private Integer nbPlace;
    private Integer total;
    private Double latitude;
    private Double longitude;
    private boolean favourite;

    public ParkingData(String id, String name, Integer nbPlace, Integer total, Double latitude, Double longitude, Context context){
        this.name = name;
        this.nbPlace = nbPlace;
        this.total = total;
        this.latitude = latitude;
        this.longitude = longitude;
        this.id = id;
        this.context = context;
        this.wrapper = new SqlWrapper(this.context);
        this.favourite = false;
        this.favourite = getFavourite();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNbPlace() {
        return nbPlace;
    }

    public void setNbPlace(Integer nbPlace) {
        this.nbPlace = nbPlace;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setFavourite(boolean favourite){
        wrapper.insertValue("favorisParking", this.id, String.valueOf(favourite));
        this.favourite = favourite;
    }
    public boolean getFavourite(){
        this.favourite = wrapper.getValues("favorisParking", this.id);
        return this.favourite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

