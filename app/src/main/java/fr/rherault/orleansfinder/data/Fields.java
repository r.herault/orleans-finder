package fr.rherault.orleansfinder.data;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fields {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dispo")
    @Expose
    private Integer dispo;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("disponibilite")
    @Expose
    private Double disponibilite;
    @SerializedName("coords")
    @Expose
    private List<Double> coords = null;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("id")
    @Expose
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDispo() {
        return dispo;
    }

    public void setDispo(Integer dispo) {
        this.dispo = dispo;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Double getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(Double disponibilite) {
        this.disponibilite = disponibilite;
    }

    public List<Double> getCoords() {
        return coords;
    }

    public void setCoords(List<Double> coords) {
        this.coords = coords;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}