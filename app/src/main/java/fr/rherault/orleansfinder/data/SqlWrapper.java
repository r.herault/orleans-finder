package fr.rherault.orleansfinder.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.List;

public class SqlWrapper extends SQLiteOpenHelper {

    private static final String DB_NAME = "OrleansFinderDB";             // Nom de la base.
    private static final String DB_TABLE_NAME1 = "favorisParc"; // Nom de la table 1
    private static final String DB_TABLE_NAME2 = "favorisParking"; // Nom de la table 2

    private SQLiteDatabase db;                              // Base de données

    private Context context;

    public SqlWrapper(Context context) {
        // Appel au constructeur qui s'occupe de créer ou ouvrir la base.
        super(context, DB_NAME, null, 3);
        this.context = context;
        // Récupération de la base de données.
        db = getWritableDatabase();
    }

    /**
     * Méthode appelée si la base n'existe pas.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DB_TABLE_NAME1+ " (idParc text not null, Favourite text not null);");
        db.execSQL("create table " + DB_TABLE_NAME2+ " (idParking text not null unique, Favourite text not null);");
    }

    /**
     * Méthode pour passer d'une version de SQLite à une nouvelle version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion) {

    }

    /**
     * Insertion d'une chaîne pour une table donnée.
     */
    public void insertValue(String table,String id, String value) {
        ContentValues content = new ContentValues();
        String sqlRequest;

        if (table.equals(DB_TABLE_NAME1)){
            content.put("idParc", value);
            sqlRequest = "INSERT or replace INTO "+DB_TABLE_NAME1+
                    "(idParking, Favourite) values('"+id+"','"+value+"')";
        }
        else{
            sqlRequest = "INSERT or REPLACE INTO "+DB_TABLE_NAME2+
                    "(idParking, Favourite) values('"+id+"','"+value+"')";
        }

        db.execSQL(sqlRequest);
        // Insertion dans la base de l'instance de ContentValues contenant la chapine.
        //db.insert(table, null, content);
    }

    /**
     * Récupération des chaînes de la table.
     */
    public boolean getValues(String table, String id) {
        List<String> list = new ArrayList<String>();
        Cursor cursor;
        //Log.i("Etat", "Nom de la table :"+table+" Nom de la variable statique "+DB_TABLE_NAME1);
        if (table.equals(DB_TABLE_NAME1)){
            String[] columns = {"idParc"};
            // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
            cursor = db.query(DB_TABLE_NAME1, columns, null, null, null, null, null);
        }
        else{
            String[] columns = {"Favourite"};
            String[] args = {id};
            // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
            cursor = db.query(DB_TABLE_NAME2, columns, "idParking=?", args, null, null, null);
        }

        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            list.add(cursor.getString(0));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();
        try{
            return Boolean.valueOf(list.get(0));
        }
        catch (IndexOutOfBoundsException e){
            return false;
        }
    }

    public void resetValue(String table) {
        String clearDBQuery = "DELETE FROM " + table;
        db.execSQL(clearDBQuery);
    }
}
