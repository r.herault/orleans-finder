package fr.rherault.orleansfinder.api;

import java.util.List;

import fr.rherault.orleansfinder.data.Parking;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ParkingApi {
    @GET("records/1.0/search")
    Call<Parking> getAllParkings(@Query("dataset") String dataset, @Query("lang") String lang, @Query("rows") Integer rows);
}
