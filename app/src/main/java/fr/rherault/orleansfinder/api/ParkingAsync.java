package fr.rherault.orleansfinder.api;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import fr.rherault.orleansfinder.R;
import fr.rherault.orleansfinder.data.Parking;
import fr.rherault.orleansfinder.data.ParkingData;
import fr.rherault.orleansfinder.data.Record;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParkingAsync extends AsyncTask {
    private Call<Parking> call;

    public AsyncResponse delegate = null;

    View view;
    Context context;
    TextView responseView;
    PullRefreshLayout layout;
    ArrayList<ParkingData> parkingDataList = new ArrayList<>();

    public interface AsyncResponse {
        void processFinish(ArrayList<ParkingData> output);
    }

    public ParkingAsync(View view, Context context, AsyncResponse delegate) {
        this.view = view;
        this.context = context;
        this.layout = view.findViewById(R.id.refreshLayout);
        this.delegate = delegate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        layout.setRefreshing(true);
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        ParkingApi parkingApi = RetrofitApi.getRetrofitApi().create(ParkingApi.class);
        this.call = parkingApi.getAllParkings("mobilite-places-disponibles-parkings-en-temps-reel", "fr", 20);

        this.call.enqueue(new Callback<Parking>() {
            @Override
            public void onResponse(Call<Parking> call, Response<Parking> response) {
                if(response.isSuccessful()) {
                    for (Record parking : response.body().getRecords()) {
                        ArrayList<String> infoParking = new ArrayList<>();

                        // Name
                        String name = parking.getFields().getName();

                        // Coords
                        List<Double> coords = parking.getFields().getCoords();
                        Double latitude = coords.get(0);
                        Double longitude = coords.get(1);

                        // Nb places
                        Integer nbPlaces = parking.getFields().getDispo();

                        // id
                        String id = parking.getFields().getId();

                        // Total places
                        Integer total = parking.getFields().getTotal();

                        ParkingData parkingCourant = new ParkingData(id, name, nbPlaces, total, latitude, longitude, context);

                        if (parkingCourant.getFavourite()) {
                            parkingDataList.add(0, parkingCourant);
                        } else {
                            parkingDataList.add(parkingCourant);
                        }
                    }
                } else {
                    Toast.makeText(context,response.errorBody().toString(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Parking> call, Throwable t) {
                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        return this.parkingDataList;
    }

    protected void onPostExecute(Object o) {
        this.delegate.processFinish(this.parkingDataList);
        layout.setRefreshing(false);
    }
}
